const fs = require('fs')
const path = require('path')
const co = require('co')
const oss = require('ali-oss')

const isDev = process.env.NODE_ENV === 'development'
// 设置上传地址
const targetDir = isDev ? 'oss/project_name/test' : 'oss/project_name/release'
const buildPath = path.resolve(__dirname, 'dist')
const replaceStr = `${__dirname}/dist/`

// 阿里云OSS配置
const store = oss({
    accessKeyId: '',
    accessKeySecret: '',
    bucket: 'anjiu',
    region: 'oss-cn-shanghai'
})

// 获取文件路径列表
const getFileList = function(dir) {
    var results = []
    var list = fs.readdirSync(dir)
    list.forEach(function(file) {
        const fileName = file
        file = dir + '/' + file
        var stat = fs.statSync(file)
        if (stat && stat.isDirectory()) {
            results = results.concat(getFileList(file))
        } else {
            results.push({
                path: file,
                ossPath: targetDir + file.replace(replaceStr, '')
            })
        }
    })
    return results
}

// 使用迭代器替换遍历并发上传任务
!(function() {
    const paths = getFileList(buildPath)
    const task = async ({ ossPath, path }) => {
        console.time('上传耗时')
        const {
            res: { requestUrls }
        } = await store.put(ossPath, path)
        console.log(requestUrls)
        console.timeEnd('上传耗时')
        runner.next()
    }
    const taskScheduler = function*(task) {
        console.time('任务总耗时')
        let count = 0

        while (count < paths.length) {
            yield task(paths[count])
            count++
        }
        console.log(`共上传${count}文件`)
        console.timeEnd('任务总耗时')
    }
    const runner = taskScheduler(task)
    runner.next()
})()
