const path = require('path')
const rootPath = path.resolve(__dirname, '../')
const srcPath = path.resolve(rootPath, './src')

const basePath = {
    rootPath: rootPath,
    srcPath: srcPath,
    entryPath: path.resolve(srcPath, './entry.js'),
    distPath: path.resolve(rootPath, './dist'),
    actionsPath: path.resolve(srcPath, './actions'),
    assetsPath: path.resolve(srcPath, './assets'),
    componentPath: path.resolve(srcPath, './components'),
    layoutsPath: path.resolve(srcPath, './layouts'),
    htmlPath: path.resolve(srcPath, './html'),
    reducersPath: path.resolve(srcPath, './reducers'),
    routePath: path.resolve(srcPath, 'routes'),
    servicesPath: path.resolve(srcPath, 'services'),
    storePath: path.resolve(srcPath, './store'),
    utilsPath: path.resolve(srcPath, './utils'),
    libPath: path.resolve(srcPath, './libs'),
    themePath: path.resolve(srcPath, './theme.less')
}
module.exports = basePath
