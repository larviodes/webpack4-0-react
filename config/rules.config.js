const basePath = require('./path.config.js')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const postcssConfig = require('./postcss.config')

let rules = [
    {
        test: /\.(js|jsx)$/,
        use: 'babel-loader?cacheDirectory',
        exclude: '/node_modules/',
        include: basePath.srcPath
    },
    {
        test: /\.(jpg|png|gif|svg)$/i,
        use: [
            {
                loader: 'url-loader',
                options: {
                    limit: 1,
                    name: './images/[hash:8].[ext]'
                }
            }
        ]
    },
    {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'url-loader',
        options: {
            limit: 10000,
            name: 'media/[name].[hash:8].[ext]'
        }
    }
]

if (process.env.NODE_ENV === 'development') {
    rules.push(
        ...[
            {
                test: /\.js$/,
                use: ["source-map-loader"],
                enforce: "pre"
            },
            {
                test: /\.less$/,
                use: [
                    'style-loader',
                    'css-loader?sourceMap&modules&localIdentName=[local]-[hash:8]',
                    {
                        loader: 'postcss-loader',
                        options: Object.assign({}, postcssConfig, {
                            sourceMap: true
                        })
                    },
                    'less-loader?sourceMap',
                    {
                        loader: 'sass-resources-loader',
                        options: {
                            resources: [basePath.themePath]
                        }
                    }
                ],
                include: basePath.srcPath
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader?sourceMap',
                    {
                        loader: 'postcss-loader',
                        options: Object.assign({}, postcssConfig, {
                            sourceMap: true
                        })
                    }
                ]
            }
        ]
    )
} else {
    rules.push(
        ...[
            {
                test: /\.less$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader?modules&localIdentName=sdk-[hash:8]',
                    {
                        loader: 'postcss-loader',
                        options: Object.assign({}, postcssConfig, {
                            sourceMap: true
                        })
                    },
                    'less-loader',
                    {
                        loader: 'sass-resources-loader',
                        options: {
                            resources: [basePath.themePath]
                        }
                    }
                ],
                include: basePath.srcPath
            },
            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    {
                        loader: 'postcss-loader',
                        options: Object.assign({}, postcssConfig, {
                            sourceMap: true
                        })
                    },
                    'less-loader'
                ]
            }
        ]
    )
}

module.exports = rules
