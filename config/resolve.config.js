const basePath = require('./path.config.js')

module.exports = {
    extensions: ['.js', '.jsx', '.css', '.less'],
    alias: {
        actions: basePath.actionsPath,
        assets: basePath.assetsPath,
        components: basePath.componentPath,
        html: basePath.htmlPath,
        reducers: basePath.reducersPath,
        layouts: basePath.layoutsPath,
        routes: basePath.routePath,
        services: basePath.servicesPath,
        store: basePath.storePath,
        utils: basePath.utilsPath,
        libs: basePath.libPath
    }
}
