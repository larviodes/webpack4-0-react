const autoprefixer = require('autoprefixer')
var px2rem = require('postcss-px2rem')

module.exports = {
    plugins: [
        autoprefixer({
            browsers: ['Firefox >= 20', 'Safari > 0', 'Chrome > 20']
        }),
        px2rem({ remUnit: 75 })
    ]
}
