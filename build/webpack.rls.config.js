const path = require('path')
const webpack = require('webpack')
const ParallelUglifyPlugin = require('webpack-parallel-uglify-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')

const resolve = require('../config/resolve.config')
const rules = require('../config/rules.config')
const basePath = require('../config/path.config')

console.log(`[==============${process.env.NODE_ENV}===============]`)

const dir_name = 'yxf_sdk_h5'

const config = {
    entry: {
        app: ['@babel/polyfill', 'normalize.css', 'whatwg-fetch', basePath.entryPath]
    },
    output: {
        path: basePath.distPath,
        filename: 'js/[name].[contenthash:8].js',
        chunkFilename: 'js/[name].[contenthash:8].js',
        publicPath:
            process.env.NODE_ENV === 'production'
                ? `https://anjiu.oss-cn-shanghai.aliyuncs.com/${dir_name}/dev/`
                : process.env.NODE_ENV === 'online'
                ? `https://cdn.anjiu.cn/${dir_name}/releases/`
                : '/'
    },
    module: {
        rules: rules
    },
    mode: 'production',
    resolve: resolve,
    plugins: [
        new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /zh-cn/),
        new MiniCssExtractPlugin({
            filename: 'css/[name].[contenthash:8].css',
            chunkFilename: 'css/[id].[contenthash:8].css'
        }),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: path.resolve(basePath.htmlPath, './index.html')
        }),
        new CleanWebpackPlugin(['dist'], {
            root: basePath.rootPath,
            verbose: true,
            dry: false
        }),
        new webpack.HashedModuleIdsPlugin({
            hashFunction: 'sha256',
            hashDigest: 'hex',
            hashDigestLength: 20
        }),

        new ParallelUglifyPlugin({
            workerCount: 4,
            uglifyJS: {
                output: {
                    beautify: false, // 不需要格式化
                    comments: false // 保留注释
                },
                compress: {
                    // 压缩
                    warnings: true, // 删除无用代码时不输出警告
                    drop_console: true, // 删除console语句
                    collapse_vars: true, // 内嵌定义了但是只有用到一次的变量
                    reduce_vars: true // 提取出出现多次但是没有定义成变量去引用的静态值
                }
            }
        }),
        // new webpack.NamedChunksPlugin(chunk => chunk.name || Array.from(chunk.modulesIterable, m => m.id).join('_')),
        new webpack.DefinePlugin({
            __ONLINE__: JSON.stringify(JSON.parse(process.env.NODE_ENV === 'online')),
            __DEV__: JSON.stringify(JSON.parse(process.env.NODE_ENV === 'development'))
        })
    ],
    optimization: {
        runtimeChunk: {
            name: 'runtime'
        },
        splitChunks: {
            cacheGroups: {
                commons: {
                    chunks: 'initial',
                    minChunks: 2,
                    maxInitialRequests: 5,
                    minSize: 0
                },
                vendor: {
                    // 将第三方模块提取出来
                    test: /node_modules/,
                    chunks: 'initial',
                    name: 'vendor',
                    priority: 10, // 优先
                    enforce: true
                }
            }
        }
    }
}

module.exports = config
