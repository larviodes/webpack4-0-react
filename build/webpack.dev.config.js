const path = require('path')
const express = require('express')
const webpack = require('webpack')
const webpackMiddleware = require('webpack-dev-middleware')
const webpackHotMiddleware = require('webpack-hot-middleware')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const basePath = require('../config/path.config.js')
const resolve = require('../config/resolve.config.js')
const rules = require('../config/rules.config.js')

const devServer = express()

const port = 7206
const config = {
    entry: [
        '@babel/polyfill',
        'normalize.css',
        'whatwg-fetch',
        'react-hot-loader/patch',
        'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000',
        basePath.entryPath
    ],
    output: {
        path: basePath.distPath,
        filename: '[name].js',
        publicPath: '/'
    },
    mode: 'development',
    module: {
        rules: rules
    },
    resolve: resolve,
    plugins: [
        new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /zh-cn/),
        // new BundleAnalyzerPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: path.resolve(basePath.htmlPath, './index.html')
        }),
        new webpack.DefinePlugin({
            __DEV__: JSON.stringify(JSON.parse(process.env.NODE_ENV === 'development')),
            __ONLINE__: JSON.stringify(JSON.parse(process.env.NODE_ENV === 'online'))
        })
    ]
}

const compiler = webpack(config)

const options = {
    noInfo: true,
    stats: {
        colors: true
    }
}

devServer.use(webpackMiddleware(compiler, options))
devServer.use(webpackHotMiddleware(compiler))
devServer.use(express.static('dist'))
devServer.use('*', (req, res, next) => {
    const filename = path.resolve(compiler.outputPath, 'index.html')
    compiler.outputFileSystem.readFile(filename, (err, result) => {
        if (err) {
            return next(err)
        }
        res.set('content-type', 'text/html')
        res.send(result)
        res.end()
    })
})
devServer.listen(port, () => {
    console.log(`webpack dev server listening on ${port}`)
})
