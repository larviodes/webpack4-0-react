import { createStore, applyMiddleware, compose } from 'redux'
import thunkMiddleware from 'redux-thunk'
import reducer from 'reducers/index.js'

const middleware = [thunkMiddleware]



const store = createStore(
    reducer,
    {},
    compose(
        applyMiddleware(...middleware),
        typeof window.__REDUX_DEVTOOLS_EXTENSION__ === 'function' && __DEV__
            ? window.__REDUX_DEVTOOLS_EXTENSION__()
            : f => f
    )
)

export default function configureStore() {
    if (module.hot) {
        module.hot.accept('../reducers/index.js', () => {
            const nextRootReducer = require('../reducers/index.js').default
            store.replaceReducer(nextRootReducer)
        })
    }
    return store
}
