import React from 'react'
import Loadable from 'react-loadable'
import { Router, Switch, Route, Redirect } from 'react-router-dom'
import { createBrowserHistory } from 'history'

window.bsHistory = createBrowserHistory()

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route
        {...rest}
        render={props =>
            sessionStorage.getItem('isLogin') ? (
                <Component {...props} />
            ) : (
                <Redirect
                    to={{
                        pathname: '/',
                        state: { from: props.location }
                    }}
                />
            )
        }
    />
)

function RouterConfig() {
    return (
        <Router history={window.bsHistory}>
            <Switch>
                <Route exact path="/" render={() => <div>Hello React!!!</div>} />
            </Switch>
        </Router>
    )
}

export default RouterConfig
