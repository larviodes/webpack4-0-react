import React from 'react'
import ReactDOM from 'react-dom'
import ErrorBoundary from 'components/ErrorBoundary'
import { AppContainer } from 'react-hot-loader'
import 'material-icons-font/material-icons-font.css'
import './style.css'
import App from './app'
import LCA from 'libs/localList'

const render = Component => {
    ReactDOM.render(
        <AppContainer>
            <ErrorBoundary>
                <Component />
            </ErrorBoundary>
        </AppContainer>,
        document.getElementById('root')
    )
}
render(App)

if (__DEV__ && module.hot) {
    module.hot.accept(() => {
        render(App)
    })
}
