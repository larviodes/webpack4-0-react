import { removeEqualIndexItem } from 'libs/utils'
import uuidv4 from 'uuid/v4'

class LocalList {
    constructor() {
        this.createList('loginPhones')
        this.createList('loginAccounts')
    }
    isExist = index => {
        return !localStorage.getItem(index)
    }
    createList = index => {
        if (this.isExist(index)) {
            this.saveList(index, [])
        }
    }
    saveList = (index, value) => {
        try {
            console.log(index, value)
            const nextValue = removeEqualIndexItem(value, 'value')
            localStorage.setItem(index, JSON.stringify(nextValue))
        } catch (err) {
            throw err
        }
    }
    getItem = index => {
        const groups = localStorage.getItem(index)
        if (!groups) {
            throw new Error(`Can't find groups name of ${index}`)
        } else {
            return JSON.parse(groups)
        }
    }
    addItem = (index, newItem) => {
        console.log(index, newItem)
        const groups = localStorage.getItem(index)
        if (!groups) {
            throw new Error(`Can't find groups name of ${index}`)
        } else {
            const DATA = JSON.parse(groups)
            DATA.push({ ...newItem, key: uuidv4() })
            this.saveList(index, DATA)
        }
    }
    contactList = (index, items) => {
        const groups = localStorage.getItem(index)
        if (!groups) {
            throw new Error(`Can't find groups name of ${index}`)
        } else {
            const groupDATA = JSON.parse(groups)
            const resovleItem = items.map(item => ({
                ...item,
                key: uuidv4()
            }))

            this.saveList(index, [...groupDATA, ...resovleItem])
        }
    }
    removeItem = (index, removeItem) => {
        const groups = localStorage.getItem(index)
        if (!groups) {
            throw new Error(`Can't find groups name of ${index}`)
        } else {
            const groupDATA = JSON.parse(groups)
            const newDATA = groupDATA.filter(i => i.key !== removeItem.key)
            this.saveList(index, newDATA)
        }
    }
}

export default new LocalList()
