import qs from 'qs'
import isEmpty from 'lodash/isEmpty'

export const removeEqualIndexItem = (items, index) => {
    let arr = items
    arr.forEach(c => {
        const res = arr.filter(i => i[index] !== c[index])
        arr = [...res, c]
    })
    return arr
}

export const removeEmptyParmas = params => {
    let filterNullParams = {}
    Object.keys(params)
        .filter(i => isNumber(params[i]) || !isEmpty(params[i]))
        .forEach(i => {
            filterNullParams[i] = params[i]
        })
    return filterNullParams
}

export const getQueryParams = queryString => {
    const str = (queryString || window.location.search).replace('?', '')
    return qs.parse(str)
}
