import qs from 'qs'
import onError from 'libs/onError'

import { removeEmptyParmas } from './utils'
import { ServerError } from './customerError'

function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response
    }
    throw new ServerError('请求错误', response.status)
}

function checkBodyCode(response) {
    const { code, errCode } = response
    const resolveCode = code || errCode
    switch (resolveCode) {
        case 0: {
            return response
        }
        case 1: {
            return response
        }
        case 1100: {
            throw new ServerError('请求错误', 406)
        }
        case 1001: {
            throw new ServerError('登陆错误', 500)
        }
        default: {
            return response
        }
    }
}

function parseJSON(response) {
    return response.json()
}

export default function request(url, options) {
    return fetch(url, {
        headers: {
            Accept: 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        },
        ...options
    })
        .then(checkStatus)
        .then(parseJSON)
        .then(checkBodyCode)
        .catch(err => {
            onError(err)
        })
}

export const POST = (url, params) => {
    return request(url, {
        method: 'POST',
        body: JSON.stringify({
            ...removeEmptyParmas(params)
        })
    })
}

export const GET = (url, params) => {
    const queryString = qs.stringify({
        ...removeEmptyParmas(params)
    })
    if (queryString != '') {
        url = `${url}?${queryString}`
    }
    return request(url, {})
}
