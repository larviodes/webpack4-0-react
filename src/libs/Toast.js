import React from 'react'
import { render } from 'react-dom'

class ToastPure extends React.Component {
    constructor() {
        super()
    }

    render() {
        const { text } = this.props

        return (
            <React.Fragment>
                <div className="toast-layout">
                    <span>{text}</span>
                </div>
            </React.Fragment>
        )
    }
}

class Toast {
    init = ElementIndex => {
        this[ElementIndex] = document.createElement('div')
        this[ElementIndex].setAttribute('data-index', ElementIndex)
        document.querySelector('body').appendChild(this[ElementIndex])
    }
    add = props => {
        const ElementIndex = Math.round(Math.random(4) * 10000)
        this.init(ElementIndex)
        render(<ToastPure {...props} />, this[ElementIndex])
        this[ElementIndex + 1] = setTimeout(() => {
            this.remove(ElementIndex)
        }, props.time || 500)
    }
    remove = ElementIndex => {
        document.querySelector('body').removeChild(this[ElementIndex])
        delete this[ElementIndex]
        clearTimeout(ElementIndex + 1)
    }
}

export default new Toast()
