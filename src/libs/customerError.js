import ExtendableError from 'es6-error'

class ServerError extends ExtendableError {
    constructor(message, code) {
        this.name = 'ServerError'
        this.message = message
        this.code = code
    }
}

export { ServerError }
