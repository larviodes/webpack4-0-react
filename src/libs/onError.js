import { ServerError } from './customerError'

const onError = async err => {
    if (err instanceof ServerError) {
        switch (err.status) {
            case 401: {
            }
            case 403: {
            }
            case 400: {
            }
            case 404: {
            }
            case 406: {
            }
            case 500: {
            }
            default: {
            }
        }
    } else if (err instanceof AuthError) {
    } else if (err instanceof TypeError) {
        throw new TypeError(err)
    } else if (err instanceof SyntaxError) {
    } else if (err instanceof ReferenceError) {
    } else {
    }
}

export default onError
