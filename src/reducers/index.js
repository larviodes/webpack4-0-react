import { combineReducers } from 'redux'
import PublicReducer from './modules/publicReducer'

const reducer = combineReducers({
    PublicData: PublicReducer
})

export default reducer
