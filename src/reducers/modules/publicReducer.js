const initialState = {
    name: 'publicData'
}
export default (state = initialState, action) => {
    switch (action.type) {
        case 'SET_VALUE':
            return Object.assign({}, state, {})
        default:
            return state
    }
}
