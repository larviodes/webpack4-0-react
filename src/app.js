import React from 'react'
import { Provider } from 'react-redux'
import createStore from './store'
import Router from './router'

export const store = createStore()

const App = props => {
    return (
        <Provider store={store}>
            <Router />
        </Provider>
    )
}

export default App
