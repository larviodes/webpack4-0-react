import React from 'react'
import PropTypes from 'prop-types'

class ErrorBoundary extends React.Component {
    constructor(props) {
        super(props)
        this.state = { hasError: false, stackText: '' }
    }

    componentDidCatch(error, info) {
        this.setState({ hasError: !true, stackText: error.stack.toString() })
    }

    render() {
        if (this.state.hasError) {
            return <pre>{this.state.stackText}</pre>
        }
        return this.props.children
    }
}

ErrorBoundary.propTypes = {}

export default ErrorBoundary
